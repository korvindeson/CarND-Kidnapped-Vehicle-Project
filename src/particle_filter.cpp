/*
 * particle_filter.cpp
 *
 *  Created on: Dec 12, 2016
 *      Author: Tiffany Huang
 */

#include <random>
#include <algorithm>
#include <iostream>
#include <numeric>
#include <math.h> 
#include <iostream>
#include <sstream>
#include <string>
#include <iterator>

#include "particle_filter.h"


using namespace std;

void ParticleFilter::init(double x, double y, double theta, double std[]) {
	// Get measurements
	// Generate 'num_particles' values to cover area with particles
	// Save to global array
	// *all weights to 1.

	default_random_engine gen;
	double std_x, std_y, std_theta; // Standard deviations for x, y, and theta
	
	std_x = std[0];
	std_y = std[1];
	std_theta = std[2];
	
	// This line creates a normal (Gaussian) distribution for x
	normal_distribution<double> dist_x(x, std_x);
	// y
	normal_distribution<double> dist_y(y, std_y);
	// theta
	normal_distribution<double> dist_theta(theta, std_theta);

	// set up number of particles
	num_particles = 100;

	for (int i = 0; i < num_particles; ++i) {
		
		// init new particle
		Particle p;
		// generate values for x,y,t
		double sample_x = dist_x(gen);
		double sample_y = dist_y(gen);
		double sample_theta = dist_theta(gen);
		// particle #
		p.id = i;
		// x
		p.x = sample_x;
		// y
		p.y = sample_y;
		// theta
		p.theta = sample_theta;
		// all baasic weights to 1
		p.weight = 1.0;
		
		// add particles to global array (yeey to ++)
		particles.push_back(p);
		// add weights to global array 
		weights.push_back(1);
	}
	is_initialized = true;

}

void ParticleFilter::prediction(double delta_t, double std_pos[], double velocity, double yaw_rate) {
	// Take elapsed time, deviations, valosity and yaw
	// ...
	// Get predictions with bycicle motion model
	
	default_random_engine gen;
	// noise generators
	normal_distribution<double> dist_x(0, std_pos[0]);
	normal_distribution<double> dist_y(0, std_pos[1]);
	normal_distribution<double> dist_theta(0, std_pos[2]);

	//double x = std_pos[0];
	for (int i = 0; i < num_particles; i++) {
		// motion model
		if (fabs(yaw_rate) < 0.00001) {
			// if we go straight
			particles[i].x += velocity * delta_t * cos(particles[i].theta);
			particles[i].y += velocity * delta_t * sin(particles[i].theta);
		}
		else {
			particles[i].x += velocity / yaw_rate * (sin(particles[i].theta + yaw_rate*delta_t) - sin(particles[i].theta));
			particles[i].y += velocity / yaw_rate * (cos(particles[i].theta) - cos(particles[i].theta + yaw_rate*delta_t));
			particles[i].theta += yaw_rate * delta_t;
		}
		// add noise
		particles[i].x += dist_x(gen);
		particles[i].y += dist_y(gen);
		particles[i].theta += dist_theta(gen);
	}
}

void ParticleFilter::dataAssociation(std::vector<LandmarkObs> predicted, std::vector<LandmarkObs>& observations) {
	// Take predictions and observations
	// ...
	// Save best prediction reference to observation's id

	for (unsigned int i = 0; i < observations.size(); i++) {
		// local copy of observation
		LandmarkObs obs = observations[i];
		// assume last element as default
		int map_ind = -1;
		// init smallest distance as big number
		double best_dist = numeric_limits<double>::max();

		// go over predictions
		for (unsigned int j = 0; j < predicted.size(); j++) {
			// local copy of prediction
			LandmarkObs pred = predicted[j];
			// calculate distance between prediction and truth
			double min_dist = dist(obs.x, obs.y, pred.x, pred.y);
			// find the predicted landmark nearest the current observed landmark
			if (min_dist < best_dist) {
				// update best distance & id
				best_dist = min_dist;
				observations[i].id = pred.id;
			}
		}
	}
}

void ParticleFilter::updateWeights(double sensor_range, double std_landmark[], 
		const std::vector<LandmarkObs> &observations, const Map &map_landmarks) {
	// TODO: Update the weights of each particle using a mult-variate Gaussian distribution. You can read
	//   more about this distribution here: https://en.wikipedia.org/wiki/Multivariate_normal_distribution
	// NOTE: The observations are given in the VEHICLE'S coordinate system. Your particles are located
	//   according to the MAP'S coordinate system. You will need to transform between the two systems.
	//   Keep in mind that this transformation requires both rotation AND translation (but no scaling).
	//   The following is a good resource for the theory:
	//   https://www.willamette.edu/~gorr/classes/GeneralGraphics/Transforms/transforms2d.htm
	//   and the following is a good resource for the actual equation to implement (look at equation 
	//   3.33
	//   http://planning.cs.uiuc.edu/node99.html
	
	// calculate variables for weights update
	double n_a = 2.0 * std_landmark[0] * std_landmark[0];
	double n_b = 2.0 * std_landmark[1] * std_landmark[1];
	double gauss = 2.0 * M_PI * std_landmark[0] * std_landmark[1];

	for (int i = 0; i < num_particles; i++) {
		
		// Transform from coordinates from vehicle to map
		vector<LandmarkObs> obs;

		for (unsigned j = 0; j < observations.size(); j++) {
			double x_new = particles[i].x + observations[j].x * cos(particles[i].theta) - observations[j].y * sin(particles[i].theta);
			double y_new = particles[i].y + observations[j].x * sin(particles[i].theta) + observations[j].y * cos(particles[i].theta);

			LandmarkObs obs_new = { observations[j].id, x_new, y_new };
			obs.push_back(obs_new);
		}
		
		// Init landmark
		vector<LandmarkObs> landmarks;
		for (unsigned j = 0; j < map_landmarks.landmark_list.size(); j++) {
			// only visible (we are searching for close ones)
			if (dist(particles[i].x, particles[i].y, map_landmarks.landmark_list[j].x_f, map_landmarks.landmark_list[j].y_f) < sensor_range) {
				// save landmark
				landmarks.push_back(LandmarkObs{ map_landmarks.landmark_list[j].id_i, 
												 map_landmarks.landmark_list[j].x_f,
												 map_landmarks.landmark_list[j].y_f });
			}
		}

		// use function above to find better options
		// with a nearest neighbour method
		dataAssociation(landmarks, obs);

		// update weights with the difference betweeen particles and truth

		weights[i] = 1;
		// reset weights
		for (int j = 0; j < particles.size(); j++) {
			particles[j].weight = 1;
		}

		for (unsigned j = 0; j<obs.size(); j++) {
			
			double p_x, p_y;
			for (int k = 0; k < landmarks.size(); k++) {
				// search for the best landmark that we identified above
				if (landmarks[k].id == obs[j].id) {
					p_x = landmarks[k].x;
					p_y = landmarks[k].y;
					break;
				}
			}
			double obs_w = 1 / gauss * exp(-(pow(p_x - obs[j].x, 2) / n_a + (pow(p_y - obs[j].y, 2) / n_b)));

			// product of this obersvation weight with total observations weight
			particles[i].weight *= obs_w;
		}
		// update weights
		weights[i] = particles[i].weight;
	}
}

void ParticleFilter::resample() {
	// TODO: Resample particles with replacement with probability proportional to their weight. 
	// NOTE: You may find std::discrete_distribution helpful here.
	//   http://en.cppreference.com/w/cpp/numeric/random/discrete_distribution
	
	default_random_engine gen;
	// create temp vector for parcicles
	vector<Particle> tmp_particles;
	// init discrete distribution
	discrete_distribution<int> index(weights.begin(), weights.end());
	
	for (unsigned i = 0; i < num_particles; i++) {
		// sample using discrete distribution (more with higher weights)
		tmp_particles.push_back(particles[index(gen)]);
	}
	// update global array
	particles = tmp_particles;
}

Particle ParticleFilter::SetAssociations(Particle particle, std::vector<int> associations, std::vector<double> sense_x, std::vector<double> sense_y)
{
	//particle: the particle to assign each listed association, and association's (x,y) world coordinates mapping to
	// associations: The landmark id that goes along with each listed association
	// sense_x: the associations x mapping already converted to world coordinates
	// sense_y: the associations y mapping already converted to world coordinates

	//Clear the previous associations
	particle.associations.clear();
	particle.sense_x.clear();
	particle.sense_y.clear();

	particle.associations= associations;
 	particle.sense_x = sense_x;
 	particle.sense_y = sense_y;

 	return particle;
}

string ParticleFilter::getAssociations(Particle best)
{
	vector<int> v = best.associations;
	stringstream ss;
    copy( v.begin(), v.end(), ostream_iterator<int>(ss, " "));
    string s = ss.str();
    s = s.substr(0, s.length()-1);  // get rid of the trailing space
    return s;
}
string ParticleFilter::getSenseX(Particle best)
{
	vector<double> v = best.sense_x;
	stringstream ss;
    copy( v.begin(), v.end(), ostream_iterator<float>(ss, " "));
    string s = ss.str();
    s = s.substr(0, s.length()-1);  // get rid of the trailing space
    return s;
}
string ParticleFilter::getSenseY(Particle best)
{
	vector<double> v = best.sense_y;
	stringstream ss;
    copy( v.begin(), v.end(), ostream_iterator<float>(ss, " "));
    string s = ss.str();
    s = s.substr(0, s.length()-1);  // get rid of the trailing space
    return s;
}
